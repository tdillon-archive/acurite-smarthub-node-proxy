# AcuRite smartHUB Node Proxy

## Why?

Setup a Node proxy to upload smartHUB data to Weather Underground after Acurite [killed their service](https://www.acurite.com/blog/extending-end-of-service-and-support-for-acurite-smarthub.html).

## Setup

Add a DNS entry in your router for host `hubapi.myacurite.com` and the IP address of the machine running this app.  This will cause all traffic from the smartHUB to the AcuRite servers to be sent to this application for processing.

`Node` and `Git` need to be installed on the machine that will run this app.

## Running

```shell
git clone https://gitlab.com/tdillon/acurite-smarthub-node-proxy.git
cd acurite-smarthub-node-proxy
npm i
sudo PORT=80 ID=<Weather Underground Device ID> PASSWORD=<Weather Underground Device Key> npm start
```

If you have a web server (e.g., lighttpd, nginx) proxying your Node app, you can set the port to something besides `80` and handle for it with the web server's configuration.

## Usage

The 2 endpoints for this app are `/weatherstation/updateweatherstation` and `/status`.
The first is only intended to be used by the smartHUB.
The second is a informational endpoint to check the status of the app.

## Permanent Setup

TODO: Setup the process to run continually (e.g., after server restart).
